package visitmekong.com.atdesignlayout.CustomView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.AttributeSet;

/**
 * Created by HauDo on 1/20/2018.
 */

public class RoundRectCornerImageView extends android.support.v7.widget.AppCompatImageView {

    public RoundRectCornerImageView(Context context) {
        super(context);
    }

    public RoundRectCornerImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RoundRectCornerImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public void setImageDrawable(Drawable drawable) {
        float radius = 0.05f;
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        RoundedBitmapDrawable rid = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
        rid.setCornerRadius(bitmap.getWidth() * radius);
        super.setImageDrawable(rid);
    }
}