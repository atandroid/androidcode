package visitmekong.com.atdesignlayout.service;

import android.util.Log;

import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import visitmekong.com.atdesignlayout.Object.ActiveAuctionsModel;
import visitmekong.com.atdesignlayout.Object.AuctionModel;
import visitmekong.com.atdesignlayout.Object.CalendarModel;
import visitmekong.com.atdesignlayout.Object.GetVideoModel.BaseResponeVideo;
import visitmekong.com.atdesignlayout.Object.GetVideoModel.DataVideo;
import visitmekong.com.atdesignlayout.Object.ResponseObject;
import visitmekong.com.atdesignlayout.Object.SignInModel;
import visitmekong.com.atdesignlayout.Object.UniversityModel;
import visitmekong.com.atdesignlayout.Object.WonBidModel;
import visitmekong.com.atdesignlayout.Util.RetrofitConfig;

/**
 * Created by HauDo on 2/3/2018.
 */

public class Command {

    private Command() {

    }

    private static Command mCommand = null;

    public static Command getInstance() {
        if (mCommand == null) {
            mCommand = new Command();
        }
        return mCommand;
    }

    public void updateProfile(MultipartBody.Part part, Callback<ResponseObject> callback) {
        MobisciLabService linkBuilder = RetrofitConfig.createLinkBuilder(MobisciLabService.class);
        Call<ResponseObject> call = linkBuilder.addProfileResource(part);
        call.enqueue(callback);
    }

    public void postImage(String token, MultipartBody.Part mull, RequestBody body, Callback<ResponseObject> callback) {
        MobisciLabService linkBuilder = RetrofitConfig.createLinkBuilder(MobisciLabService.class);
        Call<ResponseObject> call = linkBuilder.postImage(token, mull, body);
        call.enqueue(callback);
    }

    public void getAllUniversity(String token, Callback<UniversityModel> callback) {
        MobisciLabService linkBuilder = RetrofitConfig.createLinkBuilder(MobisciLabService.class);
        JsonObject universitiesObject = new JsonObject();
        universitiesObject.addProperty("access_token", token);
        Call<UniversityModel> call = linkBuilder.getUniversity(universitiesObject);
        call.enqueue(callback);
    }

    public void signIn(String username, String password, String pushyID, Callback<SignInModel> callback) {
        MobisciLabService linkBuilder = RetrofitConfig.createLinkBuilder(MobisciLabService.class);
        JsonObject signInObject = new JsonObject();
        signInObject.addProperty("username", username);
        signInObject.addProperty("password", password);
        signInObject.addProperty("pushy_id", pushyID);
        Call<SignInModel> call = linkBuilder.signIn(signInObject);
        call.enqueue(callback);
    }

    public void addBid(String accessToken, int auctionId, int bidPrice, Callback<SignInModel> callback) {
        MobisciLabService linkBuilder = RetrofitConfig.createLinkBuilder(MobisciLabService.class);
        JsonObject bidObject = new JsonObject();
        bidObject.addProperty("access_token", accessToken);
        bidObject.addProperty("auction_id", auctionId);
        bidObject.addProperty("bid_price", bidPrice);
        Call<SignInModel> call = linkBuilder.signIn(bidObject);
        call.enqueue(callback);
    }

    public void getActiveBid(String accessToken, Callback<ActiveAuctionsModel> callback) {
        MobisciLabService linkBuilder = RetrofitConfig.createLinkBuilder(MobisciLabService.class);
        JsonObject bidObject = new JsonObject();
        bidObject.addProperty("access_token", accessToken);
        Call<ActiveAuctionsModel> call = linkBuilder.getActiveBid(bidObject);
    }

    public void getWonBids(String accessToken, Callback<WonBidModel> callback) {
        MobisciLabService linkBuilder = RetrofitConfig.createLinkBuilder(MobisciLabService.class);
        JsonObject bidObject = new JsonObject();
        bidObject.addProperty("access_token", accessToken);
        Call<WonBidModel> call = linkBuilder.getWonBids(bidObject);
        call.enqueue(callback);
    }

    public void getCalendars(String accessToken, Callback<CalendarModel> callback) {
        MobisciLabService linkBuilder = RetrofitConfig.createLinkBuilder(MobisciLabService.class);
        JsonObject bidObject = new JsonObject();
        bidObject.addProperty("access_token", accessToken);
        Call<CalendarModel> call = linkBuilder.getCalendars(bidObject);
        call.enqueue(callback);
    }

    public void createAuction(String accessToken, long startTime, int duration, int initPrice, Callback<AuctionModel> callback) {
        MobisciLabService linkBuilder = RetrofitConfig.createLinkBuilder(MobisciLabService.class);
        JsonObject auctionObject = new JsonObject();

        auctionObject.addProperty("access_token", accessToken);
        auctionObject.addProperty("start_time", startTime);
        auctionObject.addProperty("duration", duration);
        auctionObject.addProperty("init_price", initPrice);

        Call<AuctionModel> call = linkBuilder.createAuction(auctionObject);
        call.enqueue(callback);
    }

    public void getVideos(Callback<BaseResponeVideo> callback) {
        MobisciLabService linkBuilder = RetrofitConfig.createLinkBuilder(MobisciLabService.class);
        Call<BaseResponeVideo> call = linkBuilder.getVideos();
        call.enqueue(callback);
    }
}
