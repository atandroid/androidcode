package visitmekong.com.atdesignlayout.service;


import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import visitmekong.com.atdesignlayout.Object.ActiveAuctionsModel;
import visitmekong.com.atdesignlayout.Object.BidModel;
import visitmekong.com.atdesignlayout.Object.AuctionModel;
import visitmekong.com.atdesignlayout.Object.CalendarModel;
import visitmekong.com.atdesignlayout.Object.GetVideoModel.BaseResponeVideo;
import visitmekong.com.atdesignlayout.Object.GetVideoModel.DataVideo;
import visitmekong.com.atdesignlayout.Object.ResponseObject;
import visitmekong.com.atdesignlayout.Object.SignInModel;
import visitmekong.com.atdesignlayout.Object.SignUpTeachModel;
import visitmekong.com.atdesignlayout.Object.UniversityModel;
import visitmekong.com.atdesignlayout.Object.WonBidModel;

/**
 * Created by Android Studio on 2/3/2018.
 */

public interface MobisciLabService {

    @Multipart
    @POST("add_profile_resource")
    Call<ResponseObject> postImage(
            @Part("access_token") String token,
            @Part MultipartBody.Part image,
            @Part("avatar") RequestBody name
    );

    //Teacher
    @POST("api/signup_teacher")
    Call<SignUpTeachModel> signUpTeacher(
            @Path("access_token") String username,
            @Path("profile") JsonObject profile);

    @POST("add_profile_resource")
    @Multipart
    Call<ResponseObject> addProfileResource(
            @Part MultipartBody.Part file
    );

    @POST("api/get_popular_universities")
    Call<UniversityModel> getUniversity(
            @Body JsonObject universitiesObject);

    @POST("api/signin")
    Call<SignInModel> signIn(
            @Body JsonObject signInObject);

    @POST("api/get_active_bids")
    Call<ActiveAuctionsModel> getActiveBid(@Body JsonObject object);

    @POST("api/bid")
    Call<BidModel> addBid(@Body JsonObject bidObject);

    @POST("api/get_won_bids")
    Call<WonBidModel> getWonBids(@Body JsonObject bidOject);

    @POST("api/get_calendar")
    Call<CalendarModel> getCalendars(@Body JsonObject bidOject);

    @POST("api/create_auction")
    Call<AuctionModel> createAuction(@Body JsonObject bidOject);

    @GET("api/get_videos")
    Call<BaseResponeVideo> getVideos();

}
