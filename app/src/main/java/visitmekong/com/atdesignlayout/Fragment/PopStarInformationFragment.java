package visitmekong.com.atdesignlayout.Fragment;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import visitmekong.com.atdesignlayout.Cache.PreferencesManager;
import visitmekong.com.atdesignlayout.Object.UniversityModel;
import visitmekong.com.atdesignlayout.R;
import visitmekong.com.atdesignlayout.Util.Contanst;
import visitmekong.com.atdesignlayout.service.Command;

/**
 * Created by Android Studio on 2/3/2018.
 */

public class PopStarInformationFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private static final int ACTION_PICK = 1;

    private Spinner mSpinner;
    private ArrayList<String> mListUniversity;
    private ArrayAdapter<String> mAdapter;
    private ImageView mImvAvatar;
    private EditText mEtEmail;
    private CheckBox mChkNative;

    private View mView;

    public PopStarInformationFragment() {
    }

    public static PopStarInformationFragment newInstance() {
        return new PopStarInformationFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_pop_star_information, container, false);

        mListUniversity = new ArrayList<>();
        mListUniversity.add("University enrolled/graduated from");
        mSpinner = mView.findViewById(R.id.spn_university);
        mImvAvatar = mView.findViewById(R.id.imgAvatar);
        mEtEmail = mView.findViewById(R.id.edtEmail);
        mChkNative = mView.findViewById(R.id.chkNative);

        mImvAvatar.setOnClickListener(this);

        mAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, mListUniversity) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);

                TextView tv = (TextView) view;
                if (position == 0) {
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                TextView tv = (TextView) view;
                if (position == 0) {
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        mSpinner.setAdapter(mAdapter);
        mSpinner.setOnItemSelectedListener(this);

        SharedPreferences preferences = getContext().getSharedPreferences(Contanst.SHARED_PREFERENCES_NAME, getContext().MODE_PRIVATE);
        String accessToken = preferences.getString(Contanst.KEY_ACCESS_TOKEN, null);

        if (accessToken != null) {
            getDataUniversity(accessToken);
        }

        return mView;
    }


    private void getDataUniversity(String token) {
        Command command = Command.getInstance();
        command.getAllUniversity(token, new Callback<UniversityModel>() {
            @Override
            public void onResponse(Call call, Response response) {
                UniversityModel object = (UniversityModel) response.body();

                List<String> list = object.getData().getUniversities();
                mListUniversity.addAll(list);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgAvatar:
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, ACTION_PICK);
                break;
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == ACTION_PICK && Activity.RESULT_OK == resultCode) {
            Uri uri = intent.getData();
            try {
                assert uri != null;
                InputStream inputStream = getContext().getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                mImvAvatar.setImageBitmap(bitmap);

                //get real path & save into share preferences
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContext().getContentResolver().query(uri,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);

                PreferencesManager.getInstance()
                        .saveStringData(Contanst.SP_AVATAR_PATH, picturePath);
                Log.e("logg", picturePath);
                cursor.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        PreferencesManager.getInstance()
                .saveStringData(Contanst.SP_SCHOOL, mListUniversity.get(i));
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onPause() {
        super.onPause();
        PreferencesManager.getInstance()
                .saveStringData(Contanst.SP_EMAIL, mEtEmail.getText().toString());
        PreferencesManager.getInstance().saveStringData(
                Contanst.SP_NAVITE, String.valueOf(mChkNative.isChecked()));
    }
}

//retry file server php & android
