package visitmekong.com.atdesignlayout.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import visitmekong.com.atdesignlayout.Adapter.AdapterVideo;
import visitmekong.com.atdesignlayout.Object.GetVideoModel.VideoObject;
import visitmekong.com.atdesignlayout.R;
import visitmekong.com.atdesignlayout.Util.SetTypeUtil;

/**
 * Created by HauDo on 1/20/2018.
 */

public class LiveFragment extends Fragment {

    private View mView;

    public static LiveFragment newInstance() {
        return new LiveFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_video, container, false);

        return mView;
    }


}
