package visitmekong.com.atdesignlayout.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import visitmekong.com.atdesignlayout.Adapter.WonBidAdapter;
import visitmekong.com.atdesignlayout.Object.WonBidModel;
import visitmekong.com.atdesignlayout.R;
import visitmekong.com.atdesignlayout.service.Command;

/**
 * Created by Au Nguyen on 3/17/2018.
 */

public class WonBidsFragment extends Fragment {

    private View mView;
    
    private WonBidAdapter mWonBidAdapter;
    private RecyclerView mRvWonBid;
    private List<WonBidModel.WonAuction> listAuctions;

    public static WonBidsFragment newInstance(){
        return new WonBidsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_won_bids, container, false);

        init();

        setData();

        return mView;
    }

    private void setData() {
        Command command = Command.getInstance();

        command.getWonBids("rdDW3KAN/EUnINygstFKo7bfGgPALWuQLIsZBCKB9ss=", new Callback<WonBidModel>() {
            @Override
            public void onResponse(Call<WonBidModel> call, Response<WonBidModel> response) {
                if (response.isSuccessful()){
                    WonBidModel model = response.body();

                    if(model.getErrCode() == 0 && model.getErrMsg().equals("Success.")){
                        List<WonBidModel.WonAuction> wonAuctions = model.getData().getWonAuctions();
                        listAuctions.addAll(wonAuctions);
                        mWonBidAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getContext(), "Loading " + model.getErrMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<WonBidModel> call, Throwable t) {

            }
        });
    }

    private void init(){
        listAuctions = new ArrayList<>();
        mWonBidAdapter = new WonBidAdapter(listAuctions, getContext());
        mRvWonBid = mView.findViewById(R.id.rv_won_bid);

        LinearLayoutManager manager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRvWonBid.setLayoutManager(manager);
        mRvWonBid.setAdapter(mWonBidAdapter);
    }

}
