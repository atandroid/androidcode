package visitmekong.com.atdesignlayout.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import visitmekong.com.atdesignlayout.Activity.AddTimeSlotActivity;
import visitmekong.com.atdesignlayout.Adapter.CalendarAdapter;
import visitmekong.com.atdesignlayout.Object.CalendarModel;
import visitmekong.com.atdesignlayout.R;
import visitmekong.com.atdesignlayout.Util.SetTypeUtil;
import visitmekong.com.atdesignlayout.service.Command;

/**
 * Created by Au Nguyen on 3/17/2018.
 */

public class CalendarFragment extends Fragment implements View.OnClickListener {

    private View mView;
    private CalendarAdapter mAdapter;
    private Button mBtnNewTimeSlot;

    private Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    public static CalendarFragment newInstance(){
        return new CalendarFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_calendar, container, false);

        setUpNewTimeSlot();

        getCalendars();

        return mView;
    }

    private void setUpNewTimeSlot() {
        mBtnNewTimeSlot = mView.findViewById(R.id.btn_new_time_slot);
        SetTypeUtil.setTypeFaceTextView(getContext(), mBtnNewTimeSlot,"Chevin-Light.otf" );

        mBtnNewTimeSlot.setOnClickListener(this);
    }

    public void getCalendars(){
        Command command = Command.getInstance();
        command.getCalendars("XhPOkMG1zACGMrYsaZbEojOXY/3hTuUCA5Pf2e9Eoyk=", new Callback<CalendarModel>() {
            @Override
            public void onResponse(Call<CalendarModel> call, Response<CalendarModel> response) {
                if (response.isSuccessful()){
                    CalendarModel model = response.body();

                    if(model.getErrCode() == 0 && model.getErrMsg().equals("Success.")){
                        List<CalendarModel.Auction> auctions = model.getData().getAuctions();
                        handelActiveBidList(auctions);
                    } else {
                        Toast.makeText(mContext, "Loading " + model.getErrMsg(), Toast.LENGTH_SHORT).show();
                    }
                    Log.e("TAG", String.valueOf(response.body()));
                } else {
                    Log.e("TAG", "Fail "  + String.valueOf(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<CalendarModel> call, Throwable t) {
                Log.e("TAG",t.getMessage());
            }
        });
    }

    private void handelActiveBidList(List<CalendarModel.Auction> auctions) {
        mAdapter = new CalendarAdapter(auctions);
        RecyclerView rvCalendar = mView.findViewById(R.id.rv_calendar);

        LinearLayoutManager manager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvCalendar.setLayoutManager(manager);
        rvCalendar.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_new_time_slot:
                startNewTimeSlot();
                break;
        }
    }

    private void startNewTimeSlot() {
        Intent intent = new Intent(this.getContext(), AddTimeSlotActivity.class);
        startActivity(intent);

        long val = 1449197329;
        Date date=new Date(val);
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
        String dateText = df2.format(date);
        Log.i("TN" , dateText);
    }
}
