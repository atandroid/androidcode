package visitmekong.com.atdesignlayout.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.io.FileNotFoundException;
import java.io.InputStream;

import visitmekong.com.atdesignlayout.Cache.PreferencesManager;
import visitmekong.com.atdesignlayout.R;
import visitmekong.com.atdesignlayout.Util.Contanst;

/**
 * Created by Android Studio on 2/3/2018.
 */

public class UpLoadCertificateFragment extends Fragment implements View.OnClickListener {

    private static final int ACTION_PICK = 1;
    private ImageView mImvDiploma;
    private Button mBtnSelectFile;


    public static UpLoadCertificateFragment newInstance() {
        return new UpLoadCertificateFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_up_load_certificate, container, false);
        mImvDiploma = view.findViewById(R.id.imgDiploma);
        mBtnSelectFile = view.findViewById(R.id.btnSelectFile);

        mBtnSelectFile.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, ACTION_PICK);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == ACTION_PICK && Activity.RESULT_OK == resultCode) {
            Uri uri = intent.getData();
            try {
                assert uri != null;
                InputStream inputStream = getContext().getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                mImvDiploma.setImageBitmap(bitmap);

                //get real path & save into share preferences
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContext().getContentResolver().query(uri,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);

                PreferencesManager.getInstance()
                        .saveStringData(Contanst.SP_DIPLOMA_PATH, picturePath);
                Log.e("logg", picturePath);

                cursor.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
