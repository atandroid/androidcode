package visitmekong.com.atdesignlayout.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import visitmekong.com.atdesignlayout.Cache.PreferencesManager;
import visitmekong.com.atdesignlayout.R;
import visitmekong.com.atdesignlayout.Util.Contanst;

public class VideoIntroductionFragment extends Fragment implements View.OnClickListener {

    private View item;
    private Button mBtnRecordVideo;
    private Button mBtnSelectFile;
    private ImageView mImvVideo;
    private String videoPath;

    public static VideoIntroductionFragment newInstance() {
        return new VideoIntroductionFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        item = inflater.inflate(R.layout.fragment_video_introduction, container, false);

        mBtnRecordVideo = item.findViewById(R.id.btnRecordVideo);
        mBtnSelectFile = item.findViewById(R.id.btnSelectFile);
        mImvVideo = item.findViewById(R.id.imgVideo);

        mBtnSelectFile.setOnClickListener(this);
        mBtnRecordVideo.setOnClickListener(this);

        return item;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRecordVideo:
                Intent intent = new Intent(android.provider.MediaStore.ACTION_VIDEO_CAPTURE);
                startActivityForResult(intent, 2222);
                break;
            case R.id.btnSelectFile:
                intent = new Intent(Intent.ACTION_PICK);
                intent.setType("video/*");
                startActivityForResult(intent, 1111);
        }

    }

    private void setThumbnailImage() {
        Bitmap bmThumbnail = ThumbnailUtils
                .createVideoThumbnail(videoPath, MediaStore.Images.Thumbnails.MICRO_KIND);
        mImvVideo.setImageBitmap(bmThumbnail);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (Activity.RESULT_OK == resultCode) {
            Uri uri = intent.getData();

            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContext().getContentResolver().query(uri,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            videoPath = cursor.getString(columnIndex);

            PreferencesManager.getInstance()
                    .saveStringData(Contanst.SP_VIDEO_PATH, videoPath);
            Log.e("logg", videoPath);
            setThumbnailImage();
            cursor.close();
        }
    }
}
