package visitmekong.com.atdesignlayout.Fragment;

import android.app.ActionBar;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import visitmekong.com.atdesignlayout.Adapter.AdapterVideo;
import visitmekong.com.atdesignlayout.Object.GetVideoModel.BaseResponeVideo;
import visitmekong.com.atdesignlayout.Object.GetVideoModel.VideoObject;
import visitmekong.com.atdesignlayout.R;
import visitmekong.com.atdesignlayout.Util.SetTypeUtil;
import visitmekong.com.atdesignlayout.service.Command;

/**
 * Created by HauDo on 1/20/2018.
 */

public class VideoFragment extends Fragment {

    private View mView;
    private AdapterVideo mAdapter;
    private LinearLayout mLineaLayout;
   
    private List<VideoObject> listDataVideo;
    private Set<String> listTitle;


    public static VideoFragment newInstance() {
        return new VideoFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_live, container, false);

        mLineaLayout = mView.findViewById(R.id.layoutParent);

        listDataVideo = new ArrayList<>();
        listTitle = new HashSet<>();

        getData();
        return mView;
    }

    private void addList() {
        LinearLayout.LayoutParams paramsTextLine = new LinearLayout.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        paramsTextLine.setMargins(27, 13, 17, 0);

        LinearLayout.LayoutParams paramsTextTitle = new LinearLayout.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
        paramsTextTitle.setMargins(26, 0, 0, 0);

        LinearLayout.LayoutParams paramsRecyclerView = new LinearLayout.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
        paramsRecyclerView.setMargins(26, 26, 0, 0);

        for (String title : listTitle) {

            RecyclerView recyclerView = new RecyclerView(getContext());
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());

            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            recyclerView.setLayoutParams(paramsRecyclerView);

            List<VideoObject> videoObjects = new ArrayList<>();

            for (VideoObject video : listDataVideo) {
                if (video.getCategory().equals(title)) {
                    videoObjects.add(video);
                }
            }

            mAdapter = new AdapterVideo(videoObjects, getContext());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(mAdapter);

            mLineaLayout.addView(createTextTitle(paramsTextTitle, title));
            mLineaLayout.addView(createTextLine(paramsTextLine));
            mLineaLayout.addView(recyclerView);
        }
    }

    private TextView createTextTitle(LinearLayout.LayoutParams paramsTextTitle, String text) {
        TextView textTitle = new TextView(getContext());
        textTitle.setLayoutParams(paramsTextTitle);
        textTitle.setTextColor(Color.parseColor("#3d667c"));
        textTitle.setText(text);
        textTitle.setAllCaps(true);
        SetTypeUtil.setTypeFaceTextView(getContext(), textTitle, "Chevin-Light.otf");
        return textTitle;
    }

    private TextView createTextLine(LinearLayout.LayoutParams paramsTextLine) {
        TextView textLine = new TextView(getContext());
        textLine.setLayoutParams(paramsTextLine);
        textLine.setBackgroundColor(Color.parseColor("#35666d"));
        textLine.setHeight(5);
        return textLine;
    }

    private void getData() {
        Command.getInstance().getVideos(new Callback<BaseResponeVideo>() {
            @Override
            public void onResponse(Call<BaseResponeVideo> call, Response<BaseResponeVideo> response) {
                if (response.isSuccessful()) {
                    listDataVideo.addAll(response.body().getDataVideo().getVideoObjects());
                    for (int i = 0; i < listDataVideo.size(); i++) {
                        listTitle.add(listDataVideo.get(i).getCategory());
                    }
                    addList();
                }
            }

            @Override
            public void onFailure(Call<BaseResponeVideo> call, Throwable t) {

            }
        });


    }
}