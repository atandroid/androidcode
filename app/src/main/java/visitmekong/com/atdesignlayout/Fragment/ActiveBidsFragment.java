package visitmekong.com.atdesignlayout.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import visitmekong.com.atdesignlayout.Adapter.ActiveBidAdapter;
import visitmekong.com.atdesignlayout.Object.ActiveAuctionsModel;
import visitmekong.com.atdesignlayout.R;
import visitmekong.com.atdesignlayout.service.Command;


public class ActiveBidsFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private ArrayList<ActiveAuctionsModel.ActiveAuction> mActiveBids;
    private ActiveBidAdapter mAdapter;
    private Context mContext = this.getContext();


    public ActiveBidsFragment() {
        // Required empty public constructor
    }

    public static ActiveBidsFragment newInstance() {
        ActiveBidsFragment fragment = new ActiveBidsFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View itemView = inflater.inflate(R.layout.fragment_active_bids, container, false);

        mRecyclerView = itemView.findViewById(R.id.rcv_active_bids);
        return itemView;
    }

    @Override
    public void onResume() {
        super.onResume();

        Command command = Command.getInstance();

        command.getActiveBid("rdDW3KAN/EUnINygstFKo7bfGgPALWuQLIsZBCKB9ss=", new Callback<ActiveAuctionsModel>() {
            @Override
            public void onResponse(Call<ActiveAuctionsModel> call, Response<ActiveAuctionsModel> response) {
                if (response.isSuccessful()){
                    ActiveAuctionsModel model = response.body();

                    if(model.getErrCode() == 0 && model.getErrMsg().equals("Success.")){
                        List<ActiveAuctionsModel.ActiveAuction> auctionList = model.getData().getActiveAuctions();
                        handelActiveBidList(auctionList);
                    } else {
                        Toast.makeText(mContext, "Loading " + model.getErrMsg(), Toast.LENGTH_SHORT).show();
                    }



                    Log.e("TAG", String.valueOf(response.body()));
                }
            }

            @Override
            public void onFailure(Call<ActiveAuctionsModel> call, Throwable t) {

            }
        });
    }


    private void handelActiveBidList(List<ActiveAuctionsModel.ActiveAuction> auctionList){
        mActiveBids = new ArrayList<>();

        mAdapter = new ActiveBidAdapter(mActiveBids, this.getContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getContext(),LinearLayoutManager.VERTICAL,false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);

        mActiveBids.addAll(auctionList);

        Log.i("TAG", ""+mActiveBids.size());
        Log.i("TAG", "Add success!");

        mAdapter.notifyDataSetChanged();
    }
}
