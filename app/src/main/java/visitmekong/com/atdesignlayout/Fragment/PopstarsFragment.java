package visitmekong.com.atdesignlayout.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import visitmekong.com.atdesignlayout.R;

/**
 * Created by HauDo on 1/20/2018.
 */

public class PopstarsFragment extends Fragment {

    private View mView;

    public static PopstarsFragment newInstance() {
        return new PopstarsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_popstars, container, false);

        return mView;
    }
}
