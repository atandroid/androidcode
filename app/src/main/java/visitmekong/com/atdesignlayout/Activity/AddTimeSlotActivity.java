package visitmekong.com.atdesignlayout.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.CalendarView;

import visitmekong.com.atdesignlayout.R;

public class AddTimeSlotActivity extends AppCompatActivity {

    private CalendarView mCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_time_slot);

        mCalendar = findViewById(R.id.calendar);

        mCalendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {

                gotoDetail(year, month, dayOfMonth);

            }
        });

    }

    private void gotoDetail(int year, int month, int day){
        Intent intent = new Intent(this, TimeSlotDetailActivity.class);
        intent.putExtra("YEAR", year);
        intent.putExtra("MONTH", month);
        intent.putExtra("DAY", day);
        startActivity(intent);
    }
}
