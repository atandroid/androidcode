package visitmekong.com.atdesignlayout.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import visitmekong.com.atdesignlayout.Object.SignInModel;
import visitmekong.com.atdesignlayout.R;
import visitmekong.com.atdesignlayout.Util.Contanst;
import visitmekong.com.atdesignlayout.service.Command;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener{
    private ProgressDialog mProgressDialog;
    private Spinner mSpinnerPrefix;
    private ArrayList<String> mListPrefix;
    private ArrayAdapter<String> mAdapterPrefix;

    private Button mBtnLogin;
    private EditText mEdtEmailOrPhone;
    private EditText mEdtPassword;
    private TextView mTvSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        findViewById(R.id.tv_sign_up).setOnClickListener(this);

        init();
        setDataPrefix();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                login();
                break;
            case R.id.tv_sign_up:
                startActivity(new Intent(SignInActivity.this, SignUpActivity.class));
                finish();
                break;
        }
    }
    private void login() {
        String emailOrPhone = mEdtEmailOrPhone.getText().toString();
        final String password = mEdtPassword.getText().toString();
        Command command = Command.getInstance();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Đang xác thực tài khoản");
        mProgressDialog.show();

        command.signIn(emailOrPhone, password, "", new Callback<SignInModel>() {
            @Override
            public void onResponse(Call<SignInModel> call, Response<SignInModel> response) {
                if (response.isSuccessful()) {
                    SignInModel signInModel = response.body();
                    if (signInModel.getErrMsg().equals("Success.")){
                        configDataOfUer(signInModel);
                        mProgressDialog.dismiss();
                        startActivity(new Intent(SignInActivity.this,MainActivity.class));
                        finish();
                    }else {
                        mProgressDialog.dismiss();
                        Toast.makeText(SignInActivity.this, "Đăng nhập thất bại!", Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<SignInModel> call, Throwable t) {
                Log.e("TAG",t.getMessage());
            }

            private void configDataOfUer(SignInModel signInModel){
                String accessToken = signInModel.getData().getAccessToken();
                Integer accountType = signInModel.getData().getProfile().getAccountType();

                String avatarLink, fullName, phone, email, city, state;
                Double gender,birthday;

                SharedPreferences preferences = getSharedPreferences(Contanst.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();

                SignInModel.Data.Profile profileModel = signInModel.getData().getProfile();
                avatarLink = (String) profileModel.getAvatarLink();
                fullName = profileModel.getLastname() + " " + profileModel.getFirstname();
                phone = profileModel.getMobile();
                email = profileModel.getEmail();
                gender = (Double) profileModel.getGender();
                city = (String) profileModel.getCity();
                state = (String) profileModel.getState();
                birthday = (Double) profileModel.getBirthday();

                Log.i("LOG", gender+"");
                Log.i("LOG", birthday+"");


                editor.putString(Contanst.KEY_ACCESS_TOKEN, accessToken);
                editor.putInt(Contanst.KEY_ACCOUNT_TYPE, accountType);
                editor.putString(Contanst.EXTRA_AVATAR, avatarLink);
                editor.putString(Contanst.EXTRA_FULL_NAME, fullName);
                editor.putString(Contanst.EXTRA_MOBILE, phone);
                editor.putString(Contanst.EXTRA_EMAIL, email);
                editor.putString(Contanst.EXTRA_GENDER, String.valueOf(gender));
                editor.putString(Contanst.EXTRA_CITY, city);
                editor.putString(Contanst.EXTRA_STATE, state);
                editor.putString(Contanst.EXTRA_BIRTHDAY, String.valueOf(birthday));
                editor.apply();
            }
        });
    }

    private void init() {
        mEdtEmailOrPhone = findViewById(R.id.edt_email_phone);
        mEdtPassword = findViewById(R.id.edt_password);
        mBtnLogin = findViewById(R.id.btn_login);
        mTvSignUp = findViewById(R.id.tv_sign_up);

        mBtnLogin.setOnClickListener(this);
        mTvSignUp.setOnClickListener(this);

        mSpinnerPrefix = findViewById(R.id.spn_prefix);
        mListPrefix = new ArrayList<>();
        mListPrefix.add("prefix");
        mAdapterPrefix = new ArrayAdapter<String>(this,
                R.layout.custom_item_spinner_prefix,
                mListPrefix) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);

                TextView tv = (TextView) view;
                if (position == 0) {
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }

                return view;
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                TextView tv = (TextView) view;
                if (position == 0) {
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }

                return view;
            }
        };

        mSpinnerPrefix.setAdapter(mAdapterPrefix);
    }

    private void setDataPrefix(){
        try {
            JSONArray jsonArray = new JSONArray(loadJSONFromAsset());

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = (JSONObject) jsonArray.get(i);
                String dial_code = object.get("dial_code").toString();
                mListPrefix.add(dial_code);
                mAdapterPrefix.notifyDataSetChanged();
            }

        } catch (JSONException e) {
            Log.i("LOGCAT", e.getMessage());
        }
    }

    private String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("countrycode.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            return null;
        }
        Log.e("TAG","test git");
        return json;
    }

}
