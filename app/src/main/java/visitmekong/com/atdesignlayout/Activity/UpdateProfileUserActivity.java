package visitmekong.com.atdesignlayout.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import visitmekong.com.atdesignlayout.Object.ProfileUpdate;
import visitmekong.com.atdesignlayout.Object.ResponseObject;
import visitmekong.com.atdesignlayout.Object.SignInModel;
import visitmekong.com.atdesignlayout.Object.UserUpdate;
import visitmekong.com.atdesignlayout.R;
import visitmekong.com.atdesignlayout.Util.Contanst;
import visitmekong.com.atdesignlayout.service.Command;

public class UpdateProfileUserActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView mAvatar;

    private EditText mFullName;
    private EditText mAccountType;
    private EditText mPhone;
    private EditText mEmail;
    private EditText mBirthday;
    private EditText mGender;
    private EditText mCity;
    private EditText mState;

    private ProgressBar mProgress;

    private String mToken;
    private String mAvatarLink;
    private String mAvatarUpdate = "";

    private boolean isEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_user);

        mProgress = findViewById(R.id.progress);

        mAvatar = findViewById(R.id.imgAvatar);

        mFullName = findViewById(R.id.edtFullName);
        mAccountType = findViewById(R.id.edtAccountType);
        mPhone = findViewById(R.id.edtCountryCode);
        mEmail = findViewById(R.id.edtEmail);
        mBirthday = findViewById(R.id.edtBirthday);
        mGender = findViewById(R.id.edtGender);
        mCity = findViewById(R.id.edtCity);
        mState = findViewById(R.id.edtState);

        mAvatar.setOnClickListener(this);
        mAvatar.setEnabled(false);

        ImageView imvBackPress = findViewById(R.id.imv_back_press);
        imvBackPress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getDataFromLogin();

    }

    private void getDataFromLogin() {
        SharedPreferences preferences = getSharedPreferences(Contanst.SHARED_PREFERENCES_NAME, MODE_PRIVATE);

        mAvatarLink = "http://mobisci-lab.com:8090/" + preferences.getString(Contanst.EXTRA_AVATAR, null);
        Log.e("TAG", mAvatarLink);
        Glide.with(this)
                .load(mAvatarLink)
                //.diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(mAvatar);

        mFullName.setText(preferences.getString(Contanst.EXTRA_FULL_NAME, null));
        mAccountType.setText(String.valueOf(preferences.getInt(Contanst.KEY_ACCOUNT_TYPE, 0)));
        mPhone.setText(preferences.getString(Contanst.EXTRA_MOBILE, null));
        mEmail.setText(preferences.getString(Contanst.EXTRA_EMAIL, null));
        mCity.setText(preferences.getString(Contanst.EXTRA_CITY, null));
        mState.setText(preferences.getString(Contanst.EXTRA_STATE, null));

        String gender = preferences.getString(Contanst.EXTRA_GENDER, null);
        if (gender != null) {
            gender = gender.substring(0, 1);
            if (Integer.parseInt(gender) == 0) {
                mGender.setText("Male");
            } else {
                mGender.setText("Female");
            }
        }


        String birthday = preferences.getString(Contanst.EXTRA_BIRTHDAY, null);
        if (birthday != null) {
            int length = birthday.length();
            birthday = birthday.substring(0, length - 2);
            length = birthday.length();
            String day, month, yeah;
            switch (length) {
                case 5:
                    day = birthday.substring(0, 1);
                    month = birthday.substring(1, 3);
                    yeah = birthday.substring(3, 5);
                    mBirthday.setText(day + "/" + month + "/" + yeah);
                    break;

                case 6:
                    day = birthday.substring(0, 2);
                    month = birthday.substring(2, 4);
                    yeah = birthday.substring(4, 6);
                    mBirthday.setText(day + "/" + month + "/" + yeah);
                    break;
            }
        }


        mToken = preferences.getString(Contanst.KEY_ACCESS_TOKEN, null);
    }

    public void updateProfile() {
        Command command = Command.getInstance();

        String firstname = mFullName.getText().toString();
        String birthday = mBirthday.getText().toString();
        String gender = mGender.getText().toString();
        String city = mCity.getText().toString();
        String state = mState.getText().toString();

       /* command.updateProfile(new UserUpdate(mToken, new ProfileUpdate(firstname,
                Integer.parseInt(birthday), gender, city, state)), new Callback<ResponseObject>() {
            @Override
            public void onResponse(Call<ResponseObject> call, Response<ResponseObject> response) {
                if (response.isSuccessful()) {
                    if (!mAvatarUpdate.equals("")) {
                        postImage(mToken, mAvatarUpdate);
                    }
                    mProgress.setVisibility(View.GONE);
                    Toast.makeText(UpdateProfileUserActivity.this, "Done!", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(UpdateProfileUserActivity.this, "Error!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseObject> call, Throwable t) {
                Log.e("logg", t.getMessage());
            }
        });*/
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                //bitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, false);
                mAvatar.setImageBitmap(bitmap);
                mAvatarUpdate = getRealPathFromURI(this, uri);
                Log.e("logg", mAvatarUpdate);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void postImage(String token, String urlImage) {
        Command command = Command.getInstance();

        File file = new File(urlImage);
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", file.getName(), reqFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "upload_test");

        command.postImage(token, body, name, new Callback<ResponseObject>() {
            @Override
            public void onResponse(Call<ResponseObject> call, Response<ResponseObject> response) {
                if (response.isSuccessful()) {
                    Log.e("logg", response.body().getmErrMsg());
                    if (response.body().getmErrMsg().equals("Success")) {
                        Toast.makeText(UpdateProfileUserActivity.this, "Done!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.e("logg", response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseObject> call, Throwable t) {
                Log.e("logg", "Failure" + t.getMessage());
            }
        });
    }


    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edi_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_edit) {
            if (!isEdit) {
                mFullName.setEnabled(true);
                mBirthday.setEnabled(true);
                mGender.setEnabled(true);
                mCity.setEnabled(true);
                mState.setEnabled(true);
                mAvatar.setEnabled(true);

                item.setTitle("Save");
                isEdit = true;
            } else {
                mFullName.setEnabled(false);
                mBirthday.setEnabled(false);
                mGender.setEnabled(false);
                mCity.setEnabled(false);
                mState.setEnabled(false);
                mAvatar.setEnabled(false);

                isEdit = false;
                item.setTitle("Edit");
                mProgress.setVisibility(View.VISIBLE);
                updateProfile();
            }
        }


        return false;
    }
}
