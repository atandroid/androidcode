package visitmekong.com.atdesignlayout.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import visitmekong.com.atdesignlayout.Adapter.ViewPagerAdapter;
import visitmekong.com.atdesignlayout.Fragment.ActiveBidsFragment;
import visitmekong.com.atdesignlayout.Fragment.CalendarFragment;
import visitmekong.com.atdesignlayout.Fragment.WonBidsFragment;
import visitmekong.com.atdesignlayout.R;
import visitmekong.com.atdesignlayout.Util.Contanst;

public class OneOnOneTeacherActivity extends AppCompatActivity implements View.OnClickListener{

    private static final int STUDENT_CODE = 0;
    private static final int TEACH_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_on_one_teacher);
        findViewById(R.id.imv_back_press).setOnClickListener(this);

        setupToolbar();

        setFragmentView();

    }

    private void setupToolbar(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void setFragmentView() {
        SharedPreferences preferences = getSharedPreferences(Contanst.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        int codeState = preferences.getInt(Contanst.KEY_ACCOUNT_TYPE,-1);
        switch (codeState) {
            case STUDENT_CODE:
                setupViewPager(0);
                break;
            case TEACH_CODE:
                setupViewPager(1);
                break;
        }
    }

    private void setupViewPager(int status) {
        ViewPager viewPager = findViewById(R.id.viewPager);
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        if (status == 0) {
            viewPagerAdapter.addFragment(ActiveBidsFragment.newInstance(), "ACTIVE BIDS");
            viewPagerAdapter.addFragment(WonBidsFragment.newInstance(), "WON BIDS");

            viewPager.setAdapter(viewPagerAdapter);
            tabLayout.setupWithViewPager(viewPager);
        } else {
            viewPagerAdapter.addFragment(ActiveBidsFragment.newInstance(), "ACTIVE BIDS");
            viewPagerAdapter.addFragment(WonBidsFragment.newInstance(), "WON BIDS");
            viewPagerAdapter.addFragment(CalendarFragment.newInstance(), "CALENDAR");

            viewPager.setAdapter(viewPagerAdapter);
            tabLayout.setupWithViewPager(viewPager);
        }
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imv_back_press:
                onBackPressed();
                break;
        }
    }
}
