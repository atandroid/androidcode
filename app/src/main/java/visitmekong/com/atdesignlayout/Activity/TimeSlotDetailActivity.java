package visitmekong.com.atdesignlayout.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import visitmekong.com.atdesignlayout.Object.AuctionModel;
import visitmekong.com.atdesignlayout.R;
import visitmekong.com.atdesignlayout.service.Command;

public class TimeSlotDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mEdtDay;
    private EditText mEdtMonth;
    private EditText mEdtYear;
    private EditText mEdtPrice;

    String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    private Spinner mSpnStartTime;
    private Spinner mSpnDuration;

    private List<String> mListStartTime;
    private List<String> mListDuration;

    private ArrayAdapter mAdapterStartTime;
    private ArrayAdapter mAdapterDuration;

    private Button mBtnAddTimeSlot;

    int year, month, day;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_slot_detail);

        setupToolbar();

        addControls();

        setData();

        addEvents();
    }

    private void addEvents() {
        mBtnAddTimeSlot.setOnClickListener(this);
    }

    private void setData() {
        Intent intent = getIntent();

        if (intent != null) {

            year = intent.getIntExtra("YEAR", -1);
            month = intent.getIntExtra("MONTH", -1);
            day = intent.getIntExtra("DAY", -1);

            mEdtYear.setText(String.valueOf(year));
            //mEdtMonth.setText(monthNames[month]);
            mEdtMonth.setText(String.valueOf(month + 1));
            mEdtDay.setText(String.valueOf(day));
        }
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void addControls() {
        mEdtDay = findViewById(R.id.edt_day);
        mEdtMonth = findViewById(R.id.edt_month);
        mEdtYear = findViewById(R.id.edt_year);
        mEdtPrice = findViewById(R.id.edt_price);
        mBtnAddTimeSlot = findViewById(R.id.btn_add_time_slot);

        mSpnStartTime = findViewById(R.id.spn_time);
        mSpnDuration = findViewById(R.id.spn_duration);

        mListStartTime = new ArrayList<>();
        mListDuration = new ArrayList<>();

        setDataListStartTime();
        setDataListDuration();

        mAdapterStartTime = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, mListStartTime);
        mAdapterDuration = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, mListDuration);

        mSpnStartTime.setAdapter(mAdapterStartTime);
        mSpnDuration.setAdapter(mAdapterDuration);
    }

    private void setDataListStartTime() {
        for (int i = 0; i < 24; i++) {
            for (int j = 0; j < 60; j += 15) {
                String time = "" + i + ":" + j;
                mListStartTime.add(time);
            }
        }
    }

    private void setDataListDuration() {
        mListDuration.add("15");
        mListDuration.add("30");
        mListDuration.add("45");
        mListDuration.add("60");
        mListDuration.add("75");
        mListDuration.add("90");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_time_slot:
                addTimeSlot();
                break;
        }
    }

    private void addTimeSlot() {
        try {
            //accessToken get from ShareReference
            String accessToken = "XhPOkMG1zACGMrYsaZbEojOXY/3hTuUCA5Pf2e9Eoyk=";
            int duration = Integer.parseInt(mSpnDuration.getSelectedItem().toString());
            int initPrice = Integer.parseInt(mEdtPrice.getText().toString());

            String startTimeString = mSpnStartTime.getSelectedItem().toString();

            String split[] = startTimeString.split(":");
            int hour = Integer.parseInt(split[0]);
            int min = Integer.parseInt(split[1]);

            Calendar calendar = Calendar.getInstance();

            calendar.set(Calendar.DAY_OF_MONTH, day);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.HOUR, hour);
            calendar.set(Calendar.MINUTE, min);


            int startTime = (int) calendar.getTimeInMillis();

            Command command = Command.getInstance();
            Log.i("TN", startTime + "");
            command.createAuction(accessToken, startTime, duration, initPrice, new Callback<AuctionModel>() {
                @Override
                public void onResponse(Call<AuctionModel> call, Response<AuctionModel> response) {
                    if (response.isSuccessful()) {
                        String mess = response.message();
                        Toast.makeText(TimeSlotDetailActivity.this, mess, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AuctionModel> call, Throwable t) {

                }
            });
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Price is a number!!!", Toast.LENGTH_SHORT).show();
        }
    }
}
