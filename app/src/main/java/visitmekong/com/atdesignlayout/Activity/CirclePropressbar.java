package visitmekong.com.atdesignlayout.Activity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import visitmekong.com.atdesignlayout.CustomView.CircularProgressBar;
import visitmekong.com.atdesignlayout.R;

public class CirclePropressbar extends AppCompatActivity {

    CircularProgressBar progressBar;
    Handler handler = new Handler();
    int pStatus = 0;
    Button btnSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circle_propressbar);

        btnSignUp = findViewById(R.id.btnSignUp);

        progressBar = findViewById(R.id.progressBar);
        progressBar.setProgressColor(Color.parseColor("#7eb5ed"));
        progressBar.setTextColor(Color.parseColor("#1792d8"));
        progressBar.setProgressWidth(20);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (pStatus <= 100) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setProgress(pStatus);
                            if (pStatus == 100) {
                                btnSignUp.setText("SIGN UP");
                                btnSignUp.setBackgroundColor(Color.parseColor("#62caff"));
                            }
                        }
                    });
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    pStatus++;
                }
            }
        }).start();
    }
}
