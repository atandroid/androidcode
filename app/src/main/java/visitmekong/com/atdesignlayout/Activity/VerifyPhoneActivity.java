package visitmekong.com.atdesignlayout.Activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.widget.TextView;

import visitmekong.com.atdesignlayout.R;

public class VerifyPhoneActivity extends AppCompatActivity implements TextView.OnEditorActionListener, TextWatcher {

    TextInputEditText edt1, edt2, edt3, edt4, edt5, edt6;
    TextView tv1, tv2, tv3, tv4, tv5, tv6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);

        setupEditText();
    }

    private void setupEditText() {
        edt1 = findViewById(R.id.edt1);
        edt2 = findViewById(R.id.edt2);
        edt3 = findViewById(R.id.edt3);
        edt4 = findViewById(R.id.edt4);
        edt5 = findViewById(R.id.edt5);
        edt6 = findViewById(R.id.edt6);

        tv1 = findViewById(R.id.tv1);
        tv2 = findViewById(R.id.tv2);
        tv3 = findViewById(R.id.tv3);
        tv4 = findViewById(R.id.tv4);
        tv5 = findViewById(R.id.tv5);
        tv6 = findViewById(R.id.tv6);

        edt1.addTextChangedListener(this);
        edt2.addTextChangedListener(this);
        edt3.addTextChangedListener(this);
        edt4.addTextChangedListener(this);
        edt5.addTextChangedListener(this);
        edt6.addTextChangedListener(this);

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (edt1.getText().toString().length() == 1 && edt1.hasFocus()) {
            edt2.requestFocus();
            edt2.setText("");
            tv1.setBackgroundColor(Color.parseColor("#2bc0e4"));
        } else if (edt2.getText().toString().length() == 1 && edt2.hasFocus()) {
            edt3.requestFocus();
            edt3.setText("");
            tv2.setBackgroundColor(Color.parseColor("#2bc0e4"));
        } else if (edt3.getText().toString().length() == 1 && edt3.hasFocus()) {
            edt4.requestFocus();
            edt4.setText("");
            tv3.setBackgroundColor(Color.parseColor("#2bc0e4"));
        } else if (edt4.getText().toString().length() == 1 && edt4.hasFocus()) {
            edt5.requestFocus();
            edt5.setText("");
            tv4.setBackgroundColor(Color.parseColor("#2bc0e4"));
        } else if (edt5.getText().toString().length() == 1 && edt5.hasFocus()) {
            edt6.requestFocus();
            edt6.setText("");
            tv5.setBackgroundColor(Color.parseColor("#2bc0e4"));
        }else if (edt6.getText().toString().length() == 1 && edt6.hasFocus()){
            tv6.setBackgroundColor(Color.parseColor("#2bc0e4"));
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        return false;
    }
}
