package visitmekong.com.atdesignlayout.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import visitmekong.com.atdesignlayout.Adapter.NavAdapter;

import visitmekong.com.atdesignlayout.Adapter.ViewPagerAdapter;
import visitmekong.com.atdesignlayout.Fragment.VideoFragment;
import visitmekong.com.atdesignlayout.Fragment.PopstarsFragment;
import visitmekong.com.atdesignlayout.Fragment.LiveFragment;
import visitmekong.com.atdesignlayout.Object.NavModel;
import visitmekong.com.atdesignlayout.R;
import visitmekong.com.atdesignlayout.Util.Contanst;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int ANONYMOUS_CODE = -1;
    private static final int STUDENT_CODE = 0;
    private static final int TEACH_CODE = 1;


    private Toolbar mToolbar;
    private NavigationView mNavigationView;
    private ImageView mImvAvatar;
    private TextView mTvStudentName;

    private ImageView mImvAvatarTech;
    private TextView mTvTechName;

    private DrawerLayout mDrawerLayout;

    private LinearLayout mNavViewAnonymous;
    private RelativeLayout mNavViewStudent;
    private RelativeLayout mNavViewTeach;

    private LinearLayout mlinearLogin;

    /*
    * Config Navigation
    * */
    private RecyclerView mRecyclerviewNav_1;
    private RecyclerView mRecyclerviewNav_2;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.LayoutManager mLayoutManager_2;
    private ArrayList<NavModel> moMdels_1;
    private ArrayList<NavModel> mModels_2;
    private NavAdapter mAdapter_1;
    private NavAdapter mAdapter_2;
    private LinearLayout mTeachWithUnipop;

    /**
     *
     * Config Navigation for Teacher
     *
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = findViewById(R.id.toolbar);
        mNavigationView = findViewById(R.id.nav_view);
        mNavViewAnonymous = findViewById(R.id.linear_nav_anonymous);
        mNavViewStudent = findViewById(R.id.relative_nav_student);
        mNavViewTeach = findViewById(R.id.relative_nav_tech);
        mImvAvatarTech = findViewById(R.id.imv_avatar_nav_tech);
        mTvTechName = findViewById(R.id.tv_tech_name);
        findViewById(R.id.tv_sign_up).setOnClickListener(this);
        mlinearLogin = findViewById(R.id.linear_login);
        mTeachWithUnipop = findViewById(R.id.nav_4);
        mImvAvatar = findViewById(R.id.imv_avatar_nav);
        mTvStudentName = findViewById(R.id.tv_student_name);
        findViewById(R.id.imv_hamburger).setOnClickListener(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mlinearLogin.setOnClickListener(this);
        mTeachWithUnipop.setOnClickListener(this);
        setupViewPager();
        setUpNavigationView();

        mDrawerLayout = findViewById(R.id.drawer_layout);
    }

    private void setupViewPager() {
        ViewPager viewPager = findViewById(R.id.viewPager);
        TabLayout tabLayout = findViewById(R.id.tabLayout);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(VideoFragment.newInstance(), "Video");
        viewPagerAdapter.addFragment(LiveFragment.newInstance(), "Live");
        viewPagerAdapter.addFragment(PopstarsFragment.newInstance(), "Popstars");

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setUpNavigationView(){
        SharedPreferences preferences = getSharedPreferences(Contanst.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        int codeState = preferences.getInt(Contanst.KEY_ACCOUNT_TYPE,-1);
        switch (codeState) {
            case ANONYMOUS_CODE:
                handleSiginWithAnonymous();
                break;
            case STUDENT_CODE:
                handleSigninWithStudent();
                break;
            case TEACH_CODE:
                handleSigninWithTeach();
                break;
        }

    }

    private void setUpNavWithStudent() {
        mRecyclerviewNav_1 = findViewById(R.id.rcv_nav_2);
        mRecyclerviewNav_2 = findViewById(R.id.rcv_nav_4);
        mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mLayoutManager_2 = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        moMdels_1 = new ArrayList<>();
        mModels_2 = new ArrayList<>();
        mAdapter_1 = new NavAdapter(moMdels_1,this);
        mAdapter_2 = new NavAdapter(mModels_2,this);

        mRecyclerviewNav_1.setLayoutManager(mLayoutManager);
        mRecyclerviewNav_2.setLayoutManager(mLayoutManager_2);
        mRecyclerviewNav_1.setHasFixedSize(true);
        mRecyclerviewNav_2.setHasFixedSize(true);

        mRecyclerviewNav_1.setAdapter(mAdapter_1);
        mRecyclerviewNav_2.setAdapter(mAdapter_2);

        moMdels_1.add(new NavModel(R.drawable.ic_20,"HOME"));
        moMdels_1.add(new NavModel(R.drawable.ic_22,"FAVORITES"));
        moMdels_1.add(new NavModel(R.drawable.ic_19,"1-ON-1"));


        mModels_2.add(new NavModel(R.drawable.ic_18,"PROFILE"));
        mModels_2.add(new NavModel(R.drawable.ic_24,"SIGN OUT"));

        mAdapter_1.notifyDataSetChanged();
        mAdapter_2.notifyDataSetChanged();
    }

    private void setUpNavWithTech(){
        mRecyclerviewNav_1 = findViewById(R.id.rcv_nav_2_tech);
        mRecyclerviewNav_2 = findViewById(R.id.rcv_nav_4_tech);
        mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mLayoutManager_2 = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        moMdels_1 = new ArrayList<>();
        mModels_2 = new ArrayList<>();
        mAdapter_1 = new NavAdapter(moMdels_1,this);
        mAdapter_2 = new NavAdapter(mModels_2,this);

        mRecyclerviewNav_1.setLayoutManager(mLayoutManager);
        mRecyclerviewNav_2.setLayoutManager(mLayoutManager_2);
        mRecyclerviewNav_1.setHasFixedSize(true);
        mRecyclerviewNav_2.setHasFixedSize(true);

        mRecyclerviewNav_1.setAdapter(mAdapter_1);
        mRecyclerviewNav_2.setAdapter(mAdapter_2);

        moMdels_1.add(new NavModel(R.drawable.ic_20,"HOME"));
        moMdels_1.add(new NavModel(R.drawable.ic_21,"BROADCAST LIVE"));
        moMdels_1.add(new NavModel(R.drawable.ic_22,"FAVORITES"));
        moMdels_1.add(new NavModel(R.drawable.ic_19,"1-ON-1"));
        moMdels_1.add(new NavModel(R.drawable.ic_23,"GIFTS"));


        mModels_2.add(new NavModel(R.drawable.ic_18,"PROFILE"));
        mModels_2.add(new NavModel(R.drawable.ic_24,"SIGN OUT"));

        mAdapter_1.notifyDataSetChanged();
        mAdapter_2.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Item left menu onClickListenner
     * */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linear_login:
                startActivity(new Intent(MainActivity.this,SignInActivity.class));
                finish();
                break;
            case R.id.nav_4: // Teach with unipop
                startActivity(new Intent(MainActivity.this, SignUpTeacherActivity.class));
                finish();
                break;
            case R.id.tv_sign_up:
                startActivity(new Intent(MainActivity.this, SignUpActivity.class));
                break;
            case R.id.imv_hamburger:
                mDrawerLayout.openDrawer(Gravity.LEFT);
                break;
        }
    }

    private void handleSiginWithAnonymous(){
        mNavViewAnonymous.setVisibility(View.VISIBLE);
        mNavViewStudent.setVisibility(View.GONE);
        mNavViewTeach.setVisibility(View.GONE);
    }

    private void handleSigninWithStudent(){
        mNavViewAnonymous.setVisibility(View.GONE);
        mNavViewStudent.setVisibility(View.VISIBLE);
        mNavViewTeach.setVisibility(View.GONE);
        setUpNavWithStudent();
        bindStudentData();
    }

    private void handleSigninWithTeach(){
        mNavViewAnonymous.setVisibility(View.GONE);
        mNavViewStudent.setVisibility(View.GONE);
        mNavViewTeach.setVisibility(View.VISIBLE);
        setUpNavWithTech();
        bindTeachData();
    }

    private void signOut(){

    }

    public void bindStudentData(){
        String url = "http://mobisci-lab.com:8090/";

        SharedPreferences preferences =  getSharedPreferences(Contanst.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        String avatarLink = preferences.getString(Contanst.EXTRA_AVATAR, null);
        String studentName = preferences.getString(Contanst.EXTRA_FULL_NAME, null);

        if( avatarLink != null && studentName != null){
            url = url + avatarLink;
            Glide.with(this.getApplicationContext())
                    .load(url)
                    .into(mImvAvatar);
            mTvStudentName.setText(studentName);
        }

    }

    public void bindTeachData(){
        String url = "http://mobisci-lab.com:8090/";

        SharedPreferences preferences =  getSharedPreferences(Contanst.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        String avatarLink = preferences.getString(Contanst.EXTRA_AVATAR, null);
        String techName = preferences.getString(Contanst.EXTRA_FULL_NAME, null);

        if( avatarLink != null && techName != null){
            url = url + avatarLink;
            Glide.with(this.getApplicationContext())
                    .load(url)
                    .into(mImvAvatarTech);
            mTvTechName.setText(techName);
        }

    }

}
