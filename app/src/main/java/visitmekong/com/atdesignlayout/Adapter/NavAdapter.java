package visitmekong.com.atdesignlayout.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

import visitmekong.com.atdesignlayout.Activity.MainActivity;
import visitmekong.com.atdesignlayout.Activity.OneOnOneTeacherActivity;
import visitmekong.com.atdesignlayout.Activity.UpdateProfileUserActivity;
import visitmekong.com.atdesignlayout.Object.NavModel;
import visitmekong.com.atdesignlayout.R;
import visitmekong.com.atdesignlayout.Util.Contanst;
import visitmekong.com.atdesignlayout.Util.SetTypeUtil;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Android Studio on 1/21/2018.
 */

public class NavAdapter extends RecyclerView.Adapter<NavAdapter.MyViewHolder> {
    private ArrayList<NavModel> models;
    private Context mContext;

    public NavAdapter(ArrayList<NavModel> models, Context mContext) {
        this.models = models;
        this.mContext = mContext;
        Log.e("TAG","TAG");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_row_menu_nav,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.mIcon.setImageResource(models.get(position).getmIconId());
        holder.mNameIcon.setText(models.get(position).getmNameNav());
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView mIcon;
        private TextView mNameIcon;
        public MyViewHolder(View itemView) {
            super(itemView);
            mIcon = itemView.findViewById(R.id.nav_ic_profile);
            mNameIcon = itemView.findViewById(R.id.nav_tv_profile);
            SetTypeUtil.setTypeFaceTextView(mContext, mNameIcon,"Chevin-Light.otf" );
            mIcon.setOnClickListener(this);
            mNameIcon.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Activity activity = (Activity) mContext;
            String caseItem = mNameIcon.getText().toString();
            switch (caseItem){
                case "HOME":
                    break;
                case "BROADCAST LIVE":
                    break;
                case "FAVORITES":
                    break;
                case "1-ON-1":
                    activity.startActivity(new Intent(activity, OneOnOneTeacherActivity.class));
                    break;
                case "GIFTS":
                    activity.startActivity(new Intent(activity, OneOnOneTeacherActivity.class));
                    break;
                case "PROFILE":
                    activity.startActivity(new Intent(activity, UpdateProfileUserActivity.class));
                    break;
                case "SIGN OUT":
                    SharedPreferences preferences = mContext.getSharedPreferences(Contanst.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();

                    editor.putInt(Contanst.KEY_ACCOUNT_TYPE, -1);
                    editor.apply();

                    activity.startActivity(new Intent(activity, MainActivity.class));
                    activity.finish();
                break;
            }
        }
    }


}
