package visitmekong.com.atdesignlayout.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import visitmekong.com.atdesignlayout.Object.ActiveAuctionsModel;
import visitmekong.com.atdesignlayout.R;

/**
 * Created by Android Studio on 3/17/2018.
 */

public class ActiveBidAdapter extends RecyclerView.Adapter<ActiveBidAdapter.MyViewHolder>{
    private ArrayList<ActiveAuctionsModel.ActiveAuction> mActiveBids;
    private Context mContext;

    public ActiveBidAdapter(ArrayList<ActiveAuctionsModel.ActiveAuction> mActiveBids, Context mContext) {
        this.mActiveBids = mActiveBids;
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_row_active_bids,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        ActiveAuctionsModel.ActiveAuction object = mActiveBids.get(position);

        String url = "http://mobisci-lab.com:8090/" + object.getOwnerProfile().getAvatarLink();

        Glide.with(mContext)
                .load(url)
                .into(holder.imgAvatar);

        holder.tvLastName.setText(object.getOwnerProfile().getFirstname() + " "
                + object.getOwnerProfile().getLastname() + " "
                + object.getStartTime());
        holder.tvDuration.setText(object.getDuration()+"");
        holder.tvBidPrice.setText(object.getBidPrice()+"");

    }

    @Override
    public int getItemCount() {
        return mActiveBids.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        //ImageView imgAvatar;
        RoundedImageView imgAvatar;
        TextView tvLastName, tvDuration, tvBidPrice;
        MyViewHolder(View itemView) {
            super(itemView);
            imgAvatar = itemView.findViewById(R.id.imv_avatar);
            tvLastName = itemView.findViewById(R.id.tv_last_name_start_time);
            tvDuration = itemView.findViewById(R.id.tv_duration);
            tvBidPrice = itemView.findViewById(R.id.tv_bid_price);
        }
    }
}
