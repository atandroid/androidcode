package visitmekong.com.atdesignlayout.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import visitmekong.com.atdesignlayout.Object.GetVideoModel.VideoObject;
import visitmekong.com.atdesignlayout.R;
import visitmekong.com.atdesignlayout.Util.SetTypeUtil;

/**
 * Created by HauDo on 1/20/2018.
 */

public class AdapterVideo extends RecyclerView.Adapter<AdapterVideo.DataViewHolder> {

    private List<VideoObject> mListVideoObject;
    private Context context;

    public AdapterVideo(List<VideoObject> mListVideoObject, Context context) {
        this.mListVideoObject = mListVideoObject;
        this.context = context;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.test, parent, false);
        return new DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        VideoObject videoObject = mListVideoObject.get(position);
        holder.txtView.setText(String.format("%s", videoObject.getTotalView()));
        holder.txtNameVideo.setText(String.format("%s", videoObject.getDescription()));
        holder.tvOwner.setText(String.format("%s", videoObject.getOwner()));

        Random random = new Random();
        int tot_seconds = videoObject.getDuration() + random.nextInt(1000);
        String hours = String.valueOf(tot_seconds / 3600);
        String minutes = String.valueOf((tot_seconds % 3600) / 60);
        String seconds = String.valueOf(tot_seconds % 60);

        minutes = minutes.length() == 1 ? "0" + minutes : minutes;
        seconds = seconds.length() == 1 ? "0" + seconds : seconds;

        if (hours.equals("0")) {
            holder.txtDuration.setText(String.format("%s:%s", minutes, seconds));
        } else {
            holder.txtDuration.setText(String.format("%s:%s:%s", hours, minutes, seconds));
        }

        Log.e("logg", videoObject.getLink());

        RequestOptions options = new RequestOptions().frame(50000);
        Glide.with(context).asBitmap()
                .load(videoObject.getLink())
                .apply(options)
                .into(holder.imvVideoAvatar);

        Glide.with(context).load("http://mobisci-lab.com/" + videoObject.getAvatar()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return mListVideoObject.size();
    }

    class DataViewHolder extends RecyclerView.ViewHolder {

        TextView txtView, tvOwner, txtDuration, txtNameVideo;
        RoundedImageView imageView, imvVideoAvatar;

        public DataViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imvAvatar);
            imvVideoAvatar = itemView.findViewById(R.id.imvVideoAvatar);
            txtView = itemView.findViewById(R.id.tvView);
            tvOwner = itemView.findViewById(R.id.tvOwner);
            txtDuration = itemView.findViewById(R.id.tvDuration);
            txtNameVideo = itemView.findViewById(R.id.tvNameVideo);

            SetTypeUtil.setTypeFaceTextView(itemView.getContext(), txtView, "Chevin-Light.otf");
            SetTypeUtil.setTypeFaceTextView(itemView.getContext(), tvOwner, "Chevin-Bold.otf");
            SetTypeUtil.setTypeFaceTextView(itemView.getContext(), txtDuration, "Chevin-Light.otf");
            SetTypeUtil.setTypeFaceTextView(itemView.getContext(), txtNameVideo, "Chevin-Light.otf");
        }
    }

}
