package visitmekong.com.atdesignlayout.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import visitmekong.com.atdesignlayout.Object.WonBidModel;
import visitmekong.com.atdesignlayout.R;
import visitmekong.com.atdesignlayout.Util.SetTypeUtil;

/**
 * Created by Au Nguyen on 3/17/2018.
 */

public class WonBidAdapter extends RecyclerView.Adapter<WonBidAdapter.DataViewHolder> {

    private List<WonBidModel.WonAuction> mListAuctions;
    private Context mContext;

    public WonBidAdapter(List<WonBidModel.WonAuction> mListAuctions, Context mContext) {
        this.mListAuctions = mListAuctions;
        this.mContext = mContext;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_won_bid, parent, false);
        return new WonBidAdapter.DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        WonBidModel.WonAuction object = mListAuctions.get(position);

        WonBidModel.OwnerProfile profile = object.getAuction().getOwnerProfile();
        String url = "http://mobisci-lab.com:8090/" + profile.getAvatarLink();
        Glide.with(mContext)
                .load(url)
                .into(holder.imgWonBid);

        holder.tvName.setText(profile.getFirstname() + " " + profile.getLastname());

        WonBidModel.Auction auction = object.getAuction();

        holder.tvDuration.setText(""+auction.getDuration());
        holder.tvBidPrice.setText(""+auction.getBidPrice());

        double dateDouble = auction.getStartTime();
        long dateLong = (long) (dateDouble);

        Date date = new Date(dateLong);
        String myDate = new SimpleDateFormat("EE,dd-MM-yyyy").format(date);
        String myTime = new SimpleDateFormat("HH:mm-aa").format(date);
        holder.tvDate.setText(myDate.toString());
        holder.tvTime.setText(myTime.toString());

        /*if (position % 3 == 0) {
            holder.btnJoin.setBackgroundResource(R.drawable.background_button_visible);
            holder.btnJoin.setClickable(true);
        } else {
            holder.btnJoin.setBackgroundResource(R.drawable.background_button_invisible);
            holder.btnJoin.setText("UPCOMING...");
            holder.btnJoin.setClickable(false);
        }*/
    }

    @Override
    public int getItemCount() {
        return mListAuctions.size();
    }

    class DataViewHolder extends RecyclerView.ViewHolder {

        RoundedImageView imgWonBid;
        TextView tvDuration;
        TextView tvName;
        TextView tvDate;
        TextView tvTime;
        TextView tvBidPrice;
        Button btnJoin;

        DataViewHolder(View itemView) {
            super(itemView);

            imgWonBid = itemView.findViewById(R.id.img_avatar);
            tvDuration = itemView.findViewById(R.id.tv_duration);
            tvName = itemView.findViewById(R.id.tv_name);
            tvDate = itemView.findViewById(R.id.tv_date);
            tvTime = itemView.findViewById(R.id.tv_time);
            tvBidPrice = itemView.findViewById(R.id.tv_bid_price);
            btnJoin = itemView.findViewById(R.id.btn_join);

            SetTypeUtil.setTypeFaceTextView(itemView.getContext(), tvDuration, "Chevin-Light.otf");
            SetTypeUtil.setTypeFaceTextView(itemView.getContext(), tvName, "Chevin-Bold.otf");
            SetTypeUtil.setTypeFaceTextView(itemView.getContext(), tvDate, "Chevin-Light.otf");
            SetTypeUtil.setTypeFaceTextView(itemView.getContext(), tvTime, "Chevin-Light.otf");
            SetTypeUtil.setTypeFaceTextView(itemView.getContext(), tvBidPrice, "Chevin-Light.otf");
            SetTypeUtil.setTypeFaceTextView(itemView.getContext(), btnJoin, "Chevin-Light.otf");

        }
    }

}
