package visitmekong.com.atdesignlayout.Adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import visitmekong.com.atdesignlayout.Object.CalendarModel;
import visitmekong.com.atdesignlayout.R;
import visitmekong.com.atdesignlayout.Util.SetTypeUtil;

/**
 * Created by Au Nguyen on 3/17/2018.
 */

public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.DataViewHolder> {

    private List<CalendarModel.Auction> mListCalendarObjects;
    private Context mContext;

    public CalendarAdapter(List<CalendarModel.Auction> mListCalendarObjects) {
        this.mListCalendarObjects = mListCalendarObjects;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_calendar, parent, false);
        mContext = parent.getContext();
        return new CalendarAdapter.DataViewHolder(view);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        CalendarModel.Auction object = mListCalendarObjects.get(position);
        CalendarModel.WinnerProfile winnerProfile = object.getAuction().getWinnerProfile();
        String url = null;
        if (winnerProfile != null) {
            url = "http://mobisci-lab.com:8090/" + winnerProfile.getAvatarLink();
            holder.tvName.setText(winnerProfile.getFirstname() + " " + winnerProfile.getLastname());
        }

        Date date=new Date(object.getAuction().getStartTime());
        SimpleDateFormat dfDate = new SimpleDateFormat("EEEE, MM dd yyyy");
        SimpleDateFormat dfTime = new SimpleDateFormat("HH:mm a");
        String dateText = dfDate.format(date);
        String dateTime = dfTime.format(date);

        holder.tvDuration.setText(object.getAuction().getDuration()+":00");
        Glide.with(mContext)
                .load(url)
                .into(holder.imgCalendar);
        holder.tvDate.setText(dateText);
        holder.tvTime.setText(dateTime);
        holder.tvBidPrice.setText(object.getAuction().getBidPrice()+"");
//        holder.tvStatus.setText(object.getmStatus());

        if (position % 2 == 0) {
            holder.btnStart.setBackgroundResource(R.drawable.background_button_visible);
            holder.btnStart.setClickable(true);
        } else {
            holder.btnStart.setBackgroundResource(R.drawable.background_button_invisible);
            holder.btnStart.setClickable(false);
        }
    }

    @Override
    public int getItemCount() {
        return mListCalendarObjects.size();
    }

    class DataViewHolder extends RecyclerView.ViewHolder {

        RoundedImageView imgCalendar;
        TextView tvDuration;
        TextView tvName;
        TextView tvDate;
        TextView tvTime;
        TextView tvBidPrice;
        TextView tvStatus;
        Button btnStart;

        DataViewHolder(View itemView) {
            super(itemView);

            imgCalendar = itemView.findViewById(R.id.img_avatar);
            tvDuration = itemView.findViewById(R.id.tv_price);
            tvName = itemView.findViewById(R.id.tv_name);
            tvDate = itemView.findViewById(R.id.tv_date);
            tvTime = itemView.findViewById(R.id.tv_time);
            tvBidPrice = itemView.findViewById(R.id.tv_bid_price);
            tvStatus = itemView.findViewById(R.id.tv_status);
            btnStart = itemView.findViewById(R.id.btn_start);

            SetTypeUtil.setTypeFaceTextView(itemView.getContext(), tvDuration, "Chevin-Light.otf");
            SetTypeUtil.setTypeFaceTextView(itemView.getContext(), tvName, "Chevin-Bold.otf");
            SetTypeUtil.setTypeFaceTextView(itemView.getContext(), tvDate, "Chevin-Light.otf");
            SetTypeUtil.setTypeFaceTextView(itemView.getContext(), tvTime, "Chevin-Light.otf");
            SetTypeUtil.setTypeFaceTextView(itemView.getContext(), tvBidPrice, "Chevin-Light.otf");
            SetTypeUtil.setTypeFaceTextView(itemView.getContext(), tvStatus, "Chevin-Light.otf");
            SetTypeUtil.setTypeFaceTextView(itemView.getContext(), btnStart, "Chevin-Light.otf");

        }
    }

}
