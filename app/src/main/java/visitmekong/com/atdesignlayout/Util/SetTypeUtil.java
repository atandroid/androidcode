package visitmekong.com.atdesignlayout.Util;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by HauDo on 1/20/2018.
 */

public class SetTypeUtil {

    public static void setTypeFaceTextView(Context context, TextView textView, String type) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), type);
        textView.setTypeface(typeface);
    }
}
