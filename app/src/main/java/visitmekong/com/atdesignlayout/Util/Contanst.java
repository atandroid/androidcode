package visitmekong.com.atdesignlayout.Util;

import android.os.Environment;

/**
 * Created by HauDo on 1/28/2018.
 */

public class Contanst {

    public static final String EXTRA_FULL_NAME = "FullName";
    public static final String EXTRA_ACCOUNT_TYPE = "AccountType";
    public static final String EXTRA_MOBILE = "Mobile";
    public static final String EXTRA_EMAIL = "Email";
    public static final String EXTRA_BIRTHDAY = "Birthday";
    public static final String EXTRA_GENDER = "Gender";
    public static final String EXTRA_CITY = "City";
    public static final String EXTRA_STATE = "State";
    public static final String EXTRA_TOKEN = "Token";
    public static final String EXTRA_AVATAR = "Avatar";

    public static final String KEY_ACCESS_TOKEN = "AccessToken";
    public static final String KEY_ACCOUNT_TYPE = "AccountType";
    public static final String SHARED_PREFERENCES_NAME = "Preferences";

    public static final String SP_AVATAR_PATH = "AvatarPath";
    public static final String SP_SCHOOL = "Sshool";
    public static final String SP_EMAIL = "Email";
    public static final String SP_NAVITE = "Navite";
    public static final String SP_DIPLOMA_PATH = "DiplomaPath";
    public static final String SP_VIDEO_PATH = "VideoPath";

    public static final String FOLDER_VIDEO = Environment.getExternalStorageState() + "/DCMI/";


}
