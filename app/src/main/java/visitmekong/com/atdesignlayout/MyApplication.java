package visitmekong.com.atdesignlayout;

import android.app.Application;

import visitmekong.com.atdesignlayout.Cache.PreferencesManager;

/**
 * Created by HauDo on 3/10/2018.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        PreferencesManager.getInstance().init(this);
    }
}
