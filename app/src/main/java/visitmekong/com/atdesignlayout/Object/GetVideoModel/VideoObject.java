package visitmekong.com.atdesignlayout.Object.GetVideoModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HauDo on 24-Mar-18.
 */

public class VideoObject {
    @SerializedName("is_liked")
    @Expose
    private int isLiked;
    @SerializedName("total_view")
    @Expose
    private int totalView;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("duration")
    @Expose
    private int duration;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("owner")
    @Expose
    private String owner;

    public int getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(int isLiked) {
        this.isLiked = isLiked;
    }

    public int getTotalView() {
        return totalView;
    }

    public void setTotalView(int totalView) {
        this.totalView = totalView;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }}
