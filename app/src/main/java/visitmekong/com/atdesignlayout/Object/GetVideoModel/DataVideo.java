package visitmekong.com.atdesignlayout.Object.GetVideoModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by HauDo on 24-Mar-18.
 */

public class DataVideo {
    @SerializedName("videos")
    @Expose
    private List<VideoObject> videoObjects;

    public List<VideoObject> getVideoObjects() {
        return videoObjects;
    }

    public void setVideoObjects(List<VideoObject> videoObjects) {
        this.videoObjects = videoObjects;
    }

}
