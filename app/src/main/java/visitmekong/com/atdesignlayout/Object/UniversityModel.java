package visitmekong.com.atdesignlayout.Object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Au Nguyen on 12/31/2017.
 */

public class UniversityModel {

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("err_msg")
    @Expose
    private String errMsg;
    @SerializedName("err_code")
    @Expose
    private Integer errCode;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public Integer getErrCode() {
        return errCode;
    }

    public void setErrCode(Integer errCode) {
        this.errCode = errCode;
    }

    public class Data {

        @SerializedName("universities")
        @Expose
        private List<String> universities;

        public List<String> getUniversities() {
            return universities;
        }

        public void setUniversities(List<String> universities) {
            this.universities = universities;
        }
    }

}

