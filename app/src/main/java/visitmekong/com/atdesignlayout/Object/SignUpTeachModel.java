package visitmekong.com.atdesignlayout.Object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Android Studio on 2/3/2018.
 */

public class SignUpTeachModel {
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("profile")
    @Expose
    private ProfileModel profileModel;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public ProfileModel getProfileModel() {
        return profileModel;
    }

    public void setProfileModel(ProfileModel profileModel) {
        this.profileModel = profileModel;
    }

}
