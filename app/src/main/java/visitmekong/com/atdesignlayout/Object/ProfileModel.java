package visitmekong.com.atdesignlayout.Object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Android Studio on 2/3/2018.
 */

public class ProfileModel {
    @SerializedName("is_native")
    @Expose
    private Integer isNative;
    @SerializedName("graduated_uni")
    @Expose
    private String graduatedUni;
    @SerializedName("edu_email")
    @Expose
    private String eduEmail;

    public Integer getIsNative() {
        return isNative;
    }

    public void setIsNative(Integer isNative) {
        this.isNative = isNative;
    }

    public String getGraduatedUni() {
        return graduatedUni;
    }

    public void setGraduatedUni(String graduatedUni) {
        this.graduatedUni = graduatedUni;
    }

    public String getEduEmail() {
        return eduEmail;
    }

    public void setEduEmail(String eduEmail) {
        this.eduEmail = eduEmail;
    }

}
