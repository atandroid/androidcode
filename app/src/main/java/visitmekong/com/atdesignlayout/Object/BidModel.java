package visitmekong.com.atdesignlayout.Object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Android Studio on 3/17/2018.
 */

public class BidModel {
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("err_code")
    @Expose
    private Integer errCode;
    @SerializedName("err_msg")
    @Expose
    private String errMsg;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getErrCode() {
        return errCode;
    }

    public void setErrCode(Integer errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    class Data {

        @SerializedName("auction")
        @Expose
        private Auction auction;

        public Auction getAuction() {
            return auction;
        }

        public void setAuction(Auction auction) {
            this.auction = auction;
        }

    }
    class Auction {

        @SerializedName("_id")
        @Expose
        private Integer id;
        @SerializedName("bid_price")
        @Expose
        private Integer bidPrice;
        @SerializedName("time_left")
        @Expose
        private Integer timeLeft;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getBidPrice() {
            return bidPrice;
        }

        public void setBidPrice(Integer bidPrice) {
            this.bidPrice = bidPrice;
        }

        public Integer getTimeLeft() {
            return timeLeft;
        }

        public void setTimeLeft(Integer timeLeft) {
            this.timeLeft = timeLeft;
        }

    }
}
