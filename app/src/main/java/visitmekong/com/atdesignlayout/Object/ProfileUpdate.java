package visitmekong.com.atdesignlayout.Object;

/**
 * Created by HauDo on 1/6/2018.
 */

public class ProfileUpdate {
    private String firstname;
    private int birthday;
    private String gender;
    private String city;
    private String state;

    public ProfileUpdate(String firstname, int birthday, String gender, String city, String state) {
        this.firstname = firstname;
        this.birthday = birthday;
        this.gender = gender;
        this.city = city;
        this.state = state;
    }
}
