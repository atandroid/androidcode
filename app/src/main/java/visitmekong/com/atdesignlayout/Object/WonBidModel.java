package visitmekong.com.atdesignlayout.Object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Android Studio on 3/18/2018.
 */

public class WonBidModel {
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("err_code")
    @Expose
    private Integer errCode;
    @SerializedName("err_msg")
    @Expose
    private String errMsg;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getErrCode() {
        return errCode;
    }

    public void setErrCode(Integer errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }


    public class Data {

        @SerializedName("won_auctions")
        @Expose
        private List<WonAuction> wonAuctions = null;

        public List<WonAuction> getWonAuctions() {
            return wonAuctions;
        }

        public void setWonAuctions(List<WonAuction> wonAuctions) {
            this.wonAuctions = wonAuctions;
        }

    }

    public class Auction {

        @SerializedName("owner_profile")
        @Expose
        private OwnerProfile ownerProfile;
        @SerializedName("_id")
        @Expose
        private Integer id;
        @SerializedName("time_left")
        @Expose
        private Integer timeLeft;
        @SerializedName("winner_id")
        @Expose
        private Integer winnerId;
        @SerializedName("duration")
        @Expose
        private Integer duration;
        @SerializedName("start_time")
        @Expose
        private Double startTime;
        @SerializedName("bid_price")
        @Expose
        private Integer bidPrice;

        public OwnerProfile getOwnerProfile() {
            return ownerProfile;
        }

        public void setOwnerProfile(OwnerProfile ownerProfile) {
            this.ownerProfile = ownerProfile;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getTimeLeft() {
            return timeLeft;
        }

        public void setTimeLeft(Integer timeLeft) {
            this.timeLeft = timeLeft;
        }

        public Integer getWinnerId() {
            return winnerId;
        }

        public void setWinnerId(Integer winnerId) {
            this.winnerId = winnerId;
        }

        public Integer getDuration() {
            return duration;
        }

        public void setDuration(Integer duration) {
            this.duration = duration;
        }

        public Double getStartTime() {
            return startTime;
        }

        public void setStartTime(Double startTime) {
            this.startTime = startTime;
        }

        public Integer getBidPrice() {
            return bidPrice;
        }

        public void setBidPrice(Integer bidPrice) {
            this.bidPrice = bidPrice;
        }

    }

    public class OwnerProfile {

        @SerializedName("_id")
        @Expose
        private Integer id;
        @SerializedName("firstname")
        @Expose
        private String firstname;
        @SerializedName("avatar_link")
        @Expose
        private String avatarLink;
        @SerializedName("lastname")
        @Expose
        private String lastname;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getAvatarLink() {
            return avatarLink;
        }

        public void setAvatarLink(String avatarLink) {
            this.avatarLink = avatarLink;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

    }

    public class WonAuction {

        @SerializedName("auction")
        @Expose
        private Auction auction;
        @SerializedName("paid")
        @Expose
        private Integer paid;

        public Auction getAuction() {
            return auction;
        }

        public void setAuction(Auction auction) {
            this.auction = auction;
        }

        public Integer getPaid() {
            return paid;
        }

        public void setPaid(Integer paid) {
            this.paid = paid;
        }

    }

}
