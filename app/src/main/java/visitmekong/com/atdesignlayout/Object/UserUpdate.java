package visitmekong.com.atdesignlayout.Object;

/**
 * Created by HauDo on 1/6/2018.
 */

public class UserUpdate {

    private String access_token;
    private ProfileUpdate profile;

    public UserUpdate(String access_token, ProfileUpdate profile) {
        this.access_token = access_token;
        this.profile = profile;
    }
}
