package visitmekong.com.atdesignlayout.Object.GetVideoModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HauDo on 24-Mar-18.
 */

public class BaseResponeVideo {
    @SerializedName("data")
    @Expose
    private DataVideo dataVideo;
    @SerializedName("err_code")
    @Expose
    private Integer errCode;
    @SerializedName("err_msg")
    @Expose
    private String errMsg;

    public DataVideo getDataVideo() {
        return dataVideo;
    }

    public void setDataVideo(DataVideo dataVideo) {
        this.dataVideo = dataVideo;
    }

    public Integer getErrCode() {
        return errCode;
    }

    public void setErrCode(Integer errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
}
