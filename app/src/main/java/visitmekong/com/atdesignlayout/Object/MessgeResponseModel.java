package visitmekong.com.atdesignlayout.Object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Android Studio on 2/3/2018.
 */

public class MessgeResponseModel {
    @SerializedName("err_msg")
    @Expose
    private String errMsg;

    @SerializedName("data")
    @Expose
    private DataModel data;

    @SerializedName("err_code")
    @Expose
    private Integer errCode;

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public DataModel getData() {
        return data;
    }

    public void setData(DataModel data) {
        this.data = data;
    }

    public Integer getErrCode() {
        return errCode;
    }

    public void setErrCode(Integer errCode) {
        this.errCode = errCode;
    }

}
