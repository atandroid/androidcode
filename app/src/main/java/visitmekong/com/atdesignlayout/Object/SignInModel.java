package visitmekong.com.atdesignlayout.Object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Au Nguyen on 12/31/2017.
 */

public class SignInModel {

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("err_msg")
    @Expose
    private String errMsg;
    @SerializedName("err_code")
    @Expose
    private Integer errCode;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public Integer getErrCode() {
        return errCode;
    }

    public void setErrCode(Integer errCode) {
        this.errCode = errCode;
    }

    public class Data {

        @SerializedName("refresh_token")
        @Expose
        private String refreshToken;
        @SerializedName("access_token")
        @Expose
        private String accessToken;
        @SerializedName("profile")
        @Expose
        private Profile profile;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("conf_token")
        @Expose
        private String confToken;

        public String getRefreshToken() {
            return refreshToken;
        }

        public void setRefreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public Profile getProfile() {
            return profile;
        }

        public void setProfile(Profile profile) {
            this.profile = profile;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getConfToken() {
            return confToken;
        }

        public void setConfToken(String confToken) {
            this.confToken = confToken;
        }

        public class Profile {

            @SerializedName("avatar_link")
            @Expose
            private Object avatarLink;
            @SerializedName("edu_mail")
            @Expose
            private Object eduMail;
            @SerializedName("user_id")
            @Expose
            private Integer userId;
            @SerializedName("birthday")
            @Expose
            private Object birthday;
            @SerializedName("lastname")
            @Expose
            private String lastname;
            @SerializedName("account_type")
            @Expose
            private Integer accountType;
            @SerializedName("video_link")
            @Expose
            private Object videoLink;
            @SerializedName("gender")
            @Expose
            private Object gender;
            @SerializedName("address")
            @Expose
            private Object address;
            @SerializedName("state")
            @Expose
            private Object state;
            @SerializedName("mobile")
            @Expose
            private String mobile;
            @SerializedName("school")
            @Expose
            private Object school;
            @SerializedName("is_native")
            @Expose
            private Integer isNative;
            @SerializedName("public_name")
            @Expose
            private String publicName;
            @SerializedName("firstname")
            @Expose
            private String firstname;
            @SerializedName("email")
            @Expose
            private String email;
            @SerializedName("city")
            @Expose
            private Object city;
            @SerializedName("diploma_link")
            @Expose
            private Object diplomaLink;
            @SerializedName("graduated_uni")
            @Expose
            private Object graduatedUni;

            public Object getAvatarLink() {
                return avatarLink;
            }

            public void setAvatarLink(Object avatarLink) {
                this.avatarLink = avatarLink;
            }

            public Object getEduMail() {
                return eduMail;
            }

            public void setEduMail(Object eduMail) {
                this.eduMail = eduMail;
            }

            public Integer getUserId() {
                return userId;
            }

            public void setUserId(Integer userId) {
                this.userId = userId;
            }

            public Object getBirthday() {
                return birthday;
            }

            public void setBirthday(Object birthday) {
                this.birthday = birthday;
            }

            public String getLastname() {
                return lastname;
            }

            public void setLastname(String lastname) {
                this.lastname = lastname;
            }

            public Integer getAccountType() {
                return accountType;
            }

            public void setAccountType(Integer accountType) {
                this.accountType = accountType;
            }

            public Object getVideoLink() {
                return videoLink;
            }

            public void setVideoLink(Object videoLink) {
                this.videoLink = videoLink;
            }

            public Object getGender() {
                return gender;
            }

            public void setGender(Object gender) {
                this.gender = gender;
            }

            public Object getAddress() {
                return address;
            }

            public void setAddress(Object address) {
                this.address = address;
            }

            public Object getState() {
                return state;
            }

            public void setState(Object state) {
                this.state = state;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public Object getSchool() {
                return school;
            }

            public void setSchool(Object school) {
                this.school = school;
            }

            public Integer getIsNative() {
                return isNative;
            }

            public void setIsNative(Integer isNative) {
                this.isNative = isNative;
            }

            public String getPublicName() {
                return publicName;
            }

            public void setPublicName(String publicName) {
                this.publicName = publicName;
            }

            public String getFirstname() {
                return firstname;
            }

            public void setFirstname(String firstname) {
                this.firstname = firstname;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public Object getCity() {
                return city;
            }

            public void setCity(Object city) {
                this.city = city;
            }

            public Object getDiplomaLink() {
                return diplomaLink;
            }

            public void setDiplomaLink(Object diplomaLink) {
                this.diplomaLink = diplomaLink;
            }

            public Object getGraduatedUni() {
                return graduatedUni;
            }

            public void setGraduatedUni(Object graduatedUni) {
                this.graduatedUni = graduatedUni;
            }

        }

    }

}
