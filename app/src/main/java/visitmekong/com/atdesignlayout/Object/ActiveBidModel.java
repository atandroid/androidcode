package visitmekong.com.atdesignlayout.Object;

/**
 * Created by Android Studio on 3/17/2018.
 */

public class ActiveBidModel {
    private String mCourseDuration;
    private int mUrlAvatar;
    private String mTitle;
    private String mContent;
    private String mCurrentBid;
    private int mTuitionFee;

    public ActiveBidModel(String mCourseDuration, int mUrlAvatar, String mTitle, String mContent, String mCurrentBid, int mTuitionFee) {
        this.mCourseDuration = mCourseDuration;
        this.mUrlAvatar = mUrlAvatar;
        this.mTitle = mTitle;
        this.mContent = mContent;
        this.mCurrentBid = mCurrentBid;
        this.mTuitionFee = mTuitionFee;
    }

    public String getmCourseDuration() {
        return mCourseDuration;
    }

    public int getmUrlAvatar() {
        return mUrlAvatar;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmContent() {
        return mContent;
    }

    public String getmCurrentBid() {
        return mCurrentBid;
    }

    public int getmTuitionFee() {
        return mTuitionFee;
    }
}
