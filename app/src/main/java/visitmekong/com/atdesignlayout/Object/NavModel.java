package visitmekong.com.atdesignlayout.Object;

/**
 * Created by Android Studio on 1/21/2018.
 */

public class NavModel {
    private int mIconId;
    private String mNameNav;

    public NavModel(int mIconId, String mNameNav) {
        this.mIconId = mIconId;
        this.mNameNav = mNameNav;
    }

    public int getmIconId() {
        return mIconId;
    }

    public void setmIconId(int mIconId) {
        this.mIconId = mIconId;
    }

    public String getmNameNav() {
        return mNameNav;
    }

    public void setmNameNav(String mNameNav) {
        this.mNameNav = mNameNav;
    }
}
