package visitmekong.com.atdesignlayout.Object;

/**
 * Created by Au Nguyen on 3/17/2018.
 */

public class CalendarObject {
    private String mName;
    private String mDate;
    private String mTime;
    private String mPrice;
    private String mBidPrice;
    private String mStatus;


    public CalendarObject() {
    }

    public CalendarObject(String mName, String mDate, String mTime, String mPrice, String mBidPrice, String mStatus) {
        this.mName = mName;
        this.mDate = mDate;
        this.mTime = mTime;
        this.mPrice = mPrice;
        this.mBidPrice = mBidPrice;
        this.mStatus = mStatus;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }

    public String getmTime() {
        return mTime;
    }

    public void setmTime(String mTime) {
        this.mTime = mTime;
    }

    public String getmPrice() {
        return mPrice;
    }

    public void setmPrice(String mPrice) {
        this.mPrice = mPrice;
    }

    public String getmBidPrice() {
        return mBidPrice;
    }

    public void setmBidPrice(String mBidPrice) {
        this.mBidPrice = mBidPrice;
    }

    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }
}
