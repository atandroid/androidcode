package visitmekong.com.atdesignlayout.Object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Android Studio on 3/18/2018.
 */

public class CalendarModel {

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("err_code")
    @Expose
    private Integer errCode;
    @SerializedName("err_msg")
    @Expose
    private String errMsg;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getErrCode() {
        return errCode;
    }

    public void setErrCode(Integer errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }


    public class Data {

        @SerializedName("auctions")
        @Expose
        private List<Auction> auctions = null;

        public List<Auction> getAuctions() {
            return auctions;
        }

        public void setAuctions(List<Auction> auctions) {
            this.auctions = auctions;
        }

    }

    public class Auction {

        @SerializedName("auction")
        @Expose
        private Auction_ auction;

        public Auction_ getAuction() {
            return auction;
        }

        public void setAuction(Auction_ auction) {
            this.auction = auction;
        }

    }

    public class Auction_ {

        @SerializedName("_id")
        @Expose
        private Integer id;
        @SerializedName("time_left")
        @Expose
        private Integer timeLeft;
        @SerializedName("winner_id")
        @Expose
        private Integer winnerId;
        @SerializedName("winner_profile")
        @Expose
        private WinnerProfile winnerProfile;
        @SerializedName("start_time")
        @Expose
        private Integer startTime;
        @SerializedName("bid_price")
        @Expose
        private Integer bidPrice;
        @SerializedName("duration")
        @Expose
        private Integer duration;
        @SerializedName("owner_id")
        @Expose
        private Integer ownerId;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getTimeLeft() {
            return timeLeft;
        }

        public void setTimeLeft(Integer timeLeft) {
            this.timeLeft = timeLeft;
        }

        public Integer getWinnerId() {
            return winnerId;
        }

        public void setWinnerId(Integer winnerId) {
            this.winnerId = winnerId;
        }

        public WinnerProfile getWinnerProfile() {
            return winnerProfile;
        }

        public void setWinnerProfile(WinnerProfile winnerProfile) {
            this.winnerProfile = winnerProfile;
        }

        public Integer getStartTime() {
            return startTime;
        }

        public void setStartTime(Integer startTime) {
            this.startTime = startTime;
        }

        public Integer getBidPrice() {
            return bidPrice;
        }

        public void setBidPrice(Integer bidPrice) {
            this.bidPrice = bidPrice;
        }

        public Integer getDuration() {
            return duration;
        }

        public void setDuration(Integer duration) {
            this.duration = duration;
        }

        public Integer getOwnerId() {
            return ownerId;
        }

        public void setOwnerId(Integer ownerId) {
            this.ownerId = ownerId;
        }

    }

    public class WinnerProfile {

        @SerializedName("_id")
        @Expose
        private Integer id;
        @SerializedName("lastname")
        @Expose
        private String lastname;
        @SerializedName("avatar_link")
        @Expose
        private String avatarLink;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("firstname")
        @Expose
        private String firstname;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getAvatarLink() {
            return avatarLink;
        }

        public void setAvatarLink(String avatarLink) {
            this.avatarLink = avatarLink;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

    }

}
