package visitmekong.com.atdesignlayout.Object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HauDo on 1/6/2018.
 */

public class ResponseObject {

    @SerializedName("err_msg")
    @Expose
    private String mErrMsg;

    public String getmErrMsg() {
        return mErrMsg;
    }

    public void setmErrMsg(String mErrMsg) {
        this.mErrMsg = mErrMsg;
    }
}
