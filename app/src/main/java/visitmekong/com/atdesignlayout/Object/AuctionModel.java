package visitmekong.com.atdesignlayout.Object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Android Studio on 3/18/2018.
 */

public class AuctionModel {
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("err_code")
    @Expose
    private Integer errCode;
    @SerializedName("err_msg")
    @Expose
    private String errMsg;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getErrCode() {
        return errCode;
    }

    public void setErrCode(Integer errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public class Data {

        @SerializedName("_id")
        @Expose
        private Integer id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

    }
}

